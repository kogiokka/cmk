# cmk

cmk - a command-line tool for building CMake projects from source.
It provides an unified interface to CMake commands.

## Prerequisites

 * Perl core modules
